﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class AIcar : MonoBehaviour
{
    [SerializeField] GameObject navPointPrefab;
    Rigidbody2D rigidbody2D;
    Timer timer;
    AITrack track;
    public float speedFactor;
    public float rotationFactor;
    Transform navPoint;
    int currentNavPoinIndex = 0;
    void Start()
    {
        track = GameObject.FindObjectOfType<AITrack>();
        timer = GameObject.FindObjectOfType<Timer>();
        navPoint = track.getNextNavPoint(0);
        rigidbody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if (!timer.isStop())
        {
            Vector3 target = navPoint.transform.position - transform.position;
            float angle = Vector3.Angle(transform.up, target);
            Vector3 cross = Vector3.Cross(transform.up, target);
            if (cross.z < 0) angle = -angle;
            transform.Rotate(0, 0, angle * rotationFactor);
            rigidbody2D.AddRelativeForce(new Vector2(0, speedFactor));
            
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.transform == navPoint)
        {
            currentNavPoinIndex++;
            currentNavPoinIndex = (int)Mathf.Repeat(currentNavPoinIndex, track.track.Length - 1);
            navPoint = track.getNextNavPoint(currentNavPoinIndex);
            
            Vector2 randomizedposition = (Vector2)navPoint.transform.position + Random.insideUnitCircle;
            GameObject gameObject = Instantiate(navPointPrefab);
            gameObject.transform.position = randomizedposition;
            navPoint = gameObject.transform;
            Destroy(gameObject, 15);
        }
    }
    public void Go()
    {
        this.enabled = true;
    }
}
