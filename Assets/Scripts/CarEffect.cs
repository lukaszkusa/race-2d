﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarEffect : MonoBehaviour
{
    [SerializeField] ParticleSystem tyresMarkParticle;
    PlayerCar playerCar;
    float angle;
    // Start is called before the first frame update
    void Start()
    {
        playerCar = (PlayerCar)GameObject.FindObjectOfType(typeof(PlayerCar));
     
    }

    // Update is called once per frame
    void Update()
    {
        angle = Vector2.Angle(transform.up, playerCar.rigidbodyPlayer.velocity);
        if (angle > 30 && Mathf.Floor(playerCar.rigidbodyPlayer.velocity.magnitude) > 0) tyresMarkParticle.emissionRate = 100; 
        else tyresMarkParticle.emissionRate = 0; ;
    }
}
