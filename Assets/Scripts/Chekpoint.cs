﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chekpoint : MonoBehaviour
{
    LapSystem lapSystem;
    // Start is called before the first frame update
    void Start()
    {
        lapSystem = (LapSystem)GameObject.FindObjectOfType(typeof(LapSystem));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player") lapSystem.checkPointCompleted(this);
    }
}
