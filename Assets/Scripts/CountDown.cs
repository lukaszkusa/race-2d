﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CountDown : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI counterText;
    [SerializeField] int countFrom;
    Timer timer;
    [SerializeField]AudioClip[] audios;
    AudioSource audioSource;
    // Start is called before the first frame update

    public void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = audios[0];
        timer = (Timer)GameObject.FindObjectOfType(typeof(Timer));
    }
    public void StartCountDown() {
        audioSource.clip = audios[0];
        gameObject.SetActive(true);
        StartCoroutine("IECountDown", countFrom);
    }

    IEnumerator IECountDown(int v)
    {
        
        timer.StopTimer();
        while(v > 0)
        {
            counterText.text = v.ToString();
            v--;
            audioSource.Play();
            yield return new WaitForSeconds(1);
        }
        audioSource.clip = audios[1];
        counterText.text = "Go !";
        audioSource.Play();
        yield return new WaitForSeconds(1);
        gameObject.SetActive(false);
        timer.RestartTimer();
    }
}
