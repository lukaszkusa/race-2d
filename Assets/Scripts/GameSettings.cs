﻿using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameSettings : MonoBehaviour
{
    public MapContainer mapContainer;

    [SerializeField] GameObject[] playerCars;
    [SerializeField] GameObject[] aIcars;
    [SerializeField] Map[] maps;
    [SerializeField] TextMeshProUGUI mapNameText;
    [SerializeField] Image mapImage;
    [SerializeField] GameObject restartPanel;
    CinemachineVirtualCamera cinemachineCamera;
    CountDown countDown;
    LapSystem lapSystem;
    int currentPlayerCarIndex = 0;
    int currentIndexMap = 0;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 0;
        countDown = (CountDown)GameObject.FindObjectOfType(typeof(CountDown));
        lapSystem = (LapSystem)GameObject.FindObjectOfType(typeof(LapSystem));
        if (lapSystem == null) Debug.LogError("Lapsystemnot find");

        mapNameText.text = maps[0].mapName;
        mapImage.sprite = maps[0].thumbnail;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void StartGame()
    {
        StartCoroutine("StartGameCoroutine");
    }
    public void RestartGame()
    {
        restartPanel.SetActive(false);
        SceneManager.UnloadSceneAsync(maps[currentIndexMap].sceneName);
        gameObject.SetActive(true);
        StartCoroutine("StartGameCoroutine");
    }
    public IEnumerator StartGameCoroutine()
    {
        Scene trackScene = SceneManager.LoadScene(maps[currentIndexMap].sceneName, new LoadSceneParameters(LoadSceneMode.Additive) );
        yield return new WaitUntil(()=>(trackScene.isLoaded));
        mapContainer = GameObject.FindObjectOfType<MapContainer>();
        cinemachineCamera = GameObject.FindObjectOfType<CinemachineVirtualCamera>();

        lapSystem.SetLapSystem(mapContainer.Chekpoints);
        SetUpPlayerCar(trackScene);
        SetUpAICar(trackScene);
        Time.timeScale = 1;
        countDown.StartCountDown();
        gameObject.SetActive(false);

        yield return new WaitForSeconds(1);

    }

    private void SetUpAICar(Scene scene)
    {
        GameObject car = Instantiate(aIcars[Random.Range(0, aIcars.Length)], mapContainer.startPoints[1].position, Quaternion.Euler(0, 0, -90));
        SceneManager.MoveGameObjectToScene(car, scene);
        LapSystem.Instance.RegisterPlayer(car,1);
        car = Instantiate(aIcars[Random.Range(0, aIcars.Length)], mapContainer.startPoints[2].position, Quaternion.Euler(0, 0, -90));
        SceneManager.MoveGameObjectToScene(car, scene);
        LapSystem.Instance.RegisterPlayer(car,2);
        car = Instantiate(aIcars[Random.Range(0, aIcars.Length)], mapContainer.startPoints[3].position, Quaternion.Euler(0, 0, -90));
        SceneManager.MoveGameObjectToScene(car, scene);
        LapSystem.Instance.RegisterPlayer(car,3);
        car = Instantiate(aIcars[Random.Range(0, aIcars.Length)], mapContainer.startPoints[4].position, Quaternion.Euler(0, 0, -90));
        SceneManager.MoveGameObjectToScene(car, scene);
        LapSystem.Instance.RegisterPlayer(car,4);
    }

    private void SetUpPlayerCar(Scene scene)
    {
        GameObject  car = Instantiate(playerCars[currentPlayerCarIndex], mapContainer.startPoints[0].position, Quaternion.Euler(0, 0, -90));
        SceneManager.MoveGameObjectToScene(car, scene);
        LapSystem.Instance.RegisterPlayer(car,0);

        cinemachineCamera.Follow = car.transform;
        cinemachineCamera.LookAt = car.transform;
    }
    public void SetPlayerCarIndex(int i)
    {
        currentPlayerCarIndex = i;
    }

    public void NextMap() 
    {

        if(currentIndexMap < maps.Length-1)currentIndexMap++;
       
        mapNameText.text = maps[currentIndexMap].mapName;
        mapImage.sprite = maps[currentIndexMap].thumbnail;
        
    }
    public void PrevMap()
    {
        if (currentIndexMap > 0) currentIndexMap--;

        mapNameText.text = maps[currentIndexMap].mapName;
        mapImage.sprite = maps[currentIndexMap].thumbnail;
    }
}
