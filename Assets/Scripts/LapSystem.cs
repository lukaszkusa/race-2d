﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LapSystem : MonoBehaviour
{
    public static LapSystem Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else Destroy(gameObject);
    }
    [SerializeField] Slider slider;
    [SerializeField] TextMeshProUGUI sliderText;
    [SerializeField] TextMeshProUGUI raceLapCounterText;
    [SerializeField] TextMeshProUGUI racePositionText;
    [SerializeField] Chekpoint[] chekpoints;
    [SerializeField] GameObject restartPanel;
    [SerializeField] Timer timer;
    public List<Tuple<int, GameObject>> players = new List<Tuple<int, GameObject>>();

    int laps = 0;
    int currentLap = 1;
    int currentCheckpointIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        timer = GameObject.FindObjectOfType<Timer>();
    }

    internal void checkPointCompleted(Chekpoint chekpoint)
    {
        if(chekpoints[currentCheckpointIndex] == chekpoint)
        {
            currentCheckpointIndex++;
         
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale == 0) return;
        Tuple<int, GameObject> tuple = LapSystem.Instance.players.Find((n) => (n.Item2.name.Contains("Player")));
        if (tuple != null)
        {
            int position = tuple.Item1;
            position++;
            racePositionText.text = "POS " + position.ToString() + "/" + players.Count.ToString();
        }

        if (currentCheckpointIndex > chekpoints.Length-1)
        {
            currentCheckpointIndex = 0;
            currentLap++;
            if (currentLap > laps) ShowRestartPanel();
            else raceLapCounterText.text = "Laps " + currentLap.ToString() + "/" + laps.ToString();

        }
    }

    private void ShowRestartPanel()
    {
       
        timer.RestartTimer();
        restartPanel.SetActive(true);
        currentLap = 1;
        Time.timeScale = 0;
        
    }

    public void SliderValueChange(float v)
    {
        sliderText.text = v.ToString();
        laps = Mathf.FloorToInt(v);
    }
    public void SetLapSystem(Chekpoint[] _chekpoints) 
    {
        players = new List<Tuple<int, GameObject>>();
        this.chekpoints = _chekpoints;
        laps = Mathf.FloorToInt(slider.value);
        raceLapCounterText.text = "Laps " + currentLap.ToString() + "/" + laps.ToString();
    }

    public void RegisterPlayer(GameObject _player, int position)
    {
        players.Add(new Tuple<int, GameObject>(position, _player));
    }
}
