﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName ="new track", menuName ="Track")]
public class Map :ScriptableObject
{
    public string mapName;
    public string sceneName;
    public Sprite thumbnail;
}
