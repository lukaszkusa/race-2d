﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Overtaking : MonoBehaviour
{
    [SerializeField]GameObject owner;
    Timer timer;
    public void Start()
    {
        timer = GameObject.FindObjectOfType<Timer>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        {
          
            
            if (collision.tag == "AICar" || collision.tag == "Player") 
            {
                System.Tuple<int, GameObject> tuple = LapSystem.Instance.players.Find((n) => (n.Item2 == collision.gameObject));
                int position = tuple.Item1;
                int indexTuple = LapSystem.Instance.players.FindIndex(x => x == tuple);
                GameObject gameObject = tuple.Item2;

                System.Tuple<int, GameObject> currentTuple = LapSystem.Instance.players.Find((n) => (n.Item2 == owner));
                int currentposition = currentTuple.Item1;
                int indexCurrentTuple = LapSystem.Instance.players.FindIndex(x => x == currentTuple);

                if (position > currentposition)
                {
                    int temp = currentposition;
                    currentposition = position;
                    position = temp;
                
                }
            


                    LapSystem.Instance.players.RemoveAt(indexTuple);
                    LapSystem.Instance.players.Insert(indexTuple, new System.Tuple<int, GameObject>(position, gameObject));

                    LapSystem.Instance.players.RemoveAt(indexCurrentTuple);
                    LapSystem.Instance.players.Insert(indexCurrentTuple, new System.Tuple<int, GameObject>(currentposition, owner));
                



            }
        }
        
    }
}
