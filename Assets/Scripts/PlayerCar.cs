﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCar : MonoBehaviour
{
    public float speedFactor;
    [SerializeField] float rotateFactor;
    Transform playerCarTransform;
    public Rigidbody2D rigidbodyPlayer;
    float vAxis;
    float hAxis;
    Timer timer;
    int position { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        timer = GameObject.FindObjectOfType<Timer>();
        rigidbodyPlayer = GetComponent<Rigidbody2D>();
        playerCarTransform = GetComponent<Transform>();
        
    }

    // Update is called once per frame
    void Update()
    {
       
    }
    private void FixedUpdate()
    {
        if (!timer.isStop())
        {
            vAxis = Input.GetAxis("Vertical");
            hAxis = Input.GetAxis("Horizontal");
            rigidbodyPlayer.AddRelativeForce(new Vector2(0, vAxis * speedFactor));
        }


        if(Mathf.Floor(rigidbodyPlayer.velocity.magnitude) > 0)playerCarTransform.Rotate(0, 0, hAxis * -rotateFactor);

    }
}
