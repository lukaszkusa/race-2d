﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedUp : MonoBehaviour
{
    // Start is called before the first frame update
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player") collision.GetComponent<PlayerCar>().speedFactor *= 2f;
        else if (collision.tag == "AICar") collision.GetComponent<AIcar>().speedFactor *= 2f;
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player") collision.GetComponent<PlayerCar>().speedFactor *= 0.5f;
        else if (collision.tag == "AICar") collision.GetComponent<AIcar>().speedFactor *= 0.5f;
    }
}
