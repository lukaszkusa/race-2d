﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Timer : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI textTimer;
    float currentTime, minutes, seconds, time;
    bool timerWasStop = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        currentTime = Time.time - time;
        minutes = currentTime / 60;
        seconds = currentTime % 60;
        if(!timerWasStop)textTimer.text = minutes.ToString("00") + ":" + seconds.ToString("00");
    }
    public void RestartTimer()
    {
        time = Time.time;
        timerWasStop = false;            
    }
    public void StopTimer()
    {
        timerWasStop = true;
    }
    public bool isStop()
    {
        return timerWasStop;
    }
}
